sink("/dev/null")
suppressPackageStartupMessages(library("DMwR"))
suppressPackageStartupMessages(library("xts"))
suppressPackageStartupMessages(library("randomForest"))
suppressPackageStartupMessages(library("nnet"))
suppressPackageStartupMessages(library("e1071"))
suppressPackageStartupMessages(library("kernlab"))
suppressPackageStartupMessages(library("earth"))

############
## Many times in this file you will see functions such as suppressPackStartupMessages()
## suppressWarnings(), and sink(). These functions are so that Galaxy does not confuse warnings
## for errors whenever you are coding your solutions. It may help to remove these suppressions
## whenever you are testing your code, but remember to put them back in whenever you
## submit your final solution or else you will be guaranteed errors.
## The sink commands will suppress any print statements or console output so that may
## be your best bet for removing and then replacing at the end.
############

###################################################
### The Available Data
###################################################
library(DMwR)
data(GSPC)



###################################################
### Handling time dependent data in R
###################################################

############
## The stock market dataset relies on time dependent data in order to make predictions
## from past observations. We put the data into an object of type 'xts' which is built
## to handle this kind of data.
## Below we create 3 sequences of time data linked to random values around the normal curve:
## 	1: A sequence of 100 days, starting from 2000-01-01
##	2: A sequence of 100 minutes, starting from 2000-01-01 at 13:00
##	3: A sequence of 3 independent dates; 2005-01-01, 2005-01-10, 2005-01-12
############
library(xts)
x1 <- xts(rnorm(100),seq(as.POSIXct("2000-01-01"),len=100,by="day")) #1
x1[1:5]
x2 <- xts(rnorm(100),seq(as.POSIXct("2000-01-01 13:00"),len=100,by="min")) #2
x2[1:4]
x3 <- xts(rnorm(3),as.Date(c('2005-01-01','2005-01-10','2005-01-12'))) #3
x3

############
## Below you can see several examples of how indexing the xts objects works by dates.
## Study the output of each to learn how they work
############
x1[as.POSIXct("2000-01-04")]
x1["2000-01-05"]
x1["20000105"]
x1["2000-04"]
x1["2000-03-27/"]
x1["2000-02-26/2000-03-03"]
x1["/20000103"]

############
## Now you try: index the above created datasets to produce the requested output 
############
index1 <- x2[] # Data for each minute after 2001-01-01 14:30 (not including 14:30)
index2 <- x3[] # The value associated with 2005-01-10
index3 <- x1[] # Data collected for each date between 2001-02-04 and 2001-02-07 inclusive


############
## Now we link time series to a larger, 5x5 data matrix of more random values
############
mts.vals <- matrix(round(rnorm(25),2),5,5)
colnames(mts.vals) <- paste('ts',1:5,sep='')
mts <- xts(mts.vals,as.POSIXct(c('2003-01-01','2003-01-04',
                    '2003-01-05','2003-01-06','2003-02-16')))
mts
mts["2003-01",c("ts2","ts5")]

############
## using the index() or coredata() commands you can pull out the timestamps or the data,
## respectively.
############
index(mts)
coredata(mts)


###################################################
### Reading the data from the CSV file
###################################################

############
## Now, let's get a more meaningful time series dataset: here we load in the S&P 500
## index dataset from the file 'sp500.csv'
############
GSPC <- as.xts(read.zoo('sp500.csv',header=T))
colnames(GSPC) <- c("Open", "High", "Low", "Close","Volume","Adjusted")


                
###################################################
### Defining the Prediction Tasks
###################################################

############
## Our goal with this dataset is to create a set of predictions for which stocks will likely
## have the most variation over the immediate next n days. (See Data Mining with R for a fuller,
## more detailed explanation of this process) 
############


############
## Below, we define a function to help us retrieve this variation. A large positive result of this function
## indicates a likely 'buy', while a largely negative results indicates a 'sell'
##
## We define the margin for this decider by the tgt.margin parameter, while the number of days
## we're measuring over is marked by n.days.
############

T.ind <- function(quotes,tgt.margin=0.025,n.days=10) {
  v <- apply(HLC(quotes),1,mean)

  r <- matrix(NA,ncol=n.days,nrow=NROW(quotes))
  for(x in 1:n.days) r[,x] <- Next(Delt(v,k=x),x)

  x <- apply(r,1,function(x) sum(x[x > tgt.margin | x < -tgt.margin]))
  if (is.xts(quotes)) xts(x,time(quotes)) else x
}


############
## Now we visualize our data using a candle chart. We load in the most recent 3 months of data from our
## GSPC dataset using the last() function as shown below.
############
candleChart(last(GSPC,'3 months'),theme='white',TA=NULL)

############
## Now we want to find the mean of each of day's High-Low-Close (HLC) values. These can be easily extracted
## with the HLC(data) call. Try finishing the function below, getting the mean value of the HLC for each row
## of p.
############
avgPrice <- function(p) apply( , , )

############
## The next piece of code adds further information to our candle chart.
############
addAvgPrice <- newTA(FUN=avgPrice,col=1,legend='AvgPrice')
addT.ind <- newTA(FUN=T.ind,col='red',legend='tgtRet')
addAvgPrice(on=1)
addT.ind()


############
## The following calls define a variety of functions we'll use to help us make our prediction model
############
myATR <- function(x) ATR(HLC(x))[,'atr']					# Average True Range
mySMI <- function(x) SMI(HLC(x[,c("High","Low","Close")]))[,'SMI']		# Stochastic Momentum Index
myADX <- function(x) ADX(HLC(x))[,'ADX']					# Welles Wilder's Directional Movement Index
myAroon <- function(x) aroon(x[,c('High','Low')])$oscillator			# Aroon Index
myBB <- function(x) BBands(HLC(x[,c("High","Low","Close")]))[,'pctB']		# Bollinger Bands
myChaikinVol <- function(x) Delt(chaikinVolatility(x[,c("High","Low")]))[,1]	# Chaikin Volatility
myCLV <- function(x) EMA(CLV(HLC(x)))[,1]					# Close Location Value
myEMV <- function(x) EMV(x[,c('High','Low')],x[,'Volume'])[,2]			# Ease of Movement Value
myMFI <- function(x) MFI(x[,c("High","Low","Close")], x[,"Volume"])		# Money Flow Index
mySAR <- function(x) SAR(x[,c('High','Close')]) [,1]				# Parabolic Stop-and-Reverse
myVolat <- function(x) volatility(OHLC(x),calc="garman")[,1]			# Volatility Indicator
myMACD <- function(x) MACD(Cl(x))[,2]						# MACD oscillator


############
## Now we will create the random forest model from the above predictors
############
library(randomForest)
suppressWarnings(data.model <- specifyModel(T.ind(GSPC) ~ Delt(Cl(GSPC),k=1:10) + 
       myATR(GSPC) + mySMI(GSPC) + myADX(GSPC) + myAroon(GSPC) + 
       myBB(GSPC)  + myChaikinVol(GSPC) + myCLV(GSPC) + 
       CMO(Cl(GSPC)) + EMA(Delt(Cl(GSPC))) + myEMV(GSPC) + 
       myVolat(GSPC)  + myMFI(GSPC) + RSI(Cl(GSPC)) +
       mySAR(GSPC) + runMean(Cl(GSPC)) + runSD(Cl(GSPC)) + myMACD(GSPC)))


############
## We want to build the model, using the data.model created above. The function to do so, buildModel
## has a parameter trianing.per that in our case expects a vector of 2 dates, the beginning and end
## of the period we want to model. 
## Create the vector to get make the model from 1970-01-02 to 1999-12-31. 
## NOTE: remember to reset the seed if running this multiple times, to keep your results consistent with ours
############
set.seed(1234)
dateVector <- c( , )
rf <- buildModel(data.model,method='randomForest',
             training.per=dateVector,
             ntree=50, importance=T)


suppressWarnings(ex.model <- specifyModel(T.ind(IBM) ~ Delt(Cl(IBM),k=1:3)))
data <- modelData(ex.model,data.window=c('2009-01-01','2009-08-10'))
varImpPlot(rf@fitted.model,type=1)

############
## We can retrieve the importance of each predictor using the importance function as seen below.
## The second line of code outputs a list of all the prediction functions that had an importance
## of more than 10.
############
imp <- importance(rf@fitted.model,type=1)
rownames(imp)[which(imp > 10)]

############
## Create a new data model using only those functions that scored above 10. 
## We have taken care of the Delt(...) call, you just need to add the rest of the functions.
## HINT: It will look very similar to the data.model definition from earlier.
############
data.model <- specifyModel(T.ind(GSPC) ~ Delt(Cl(GSPC),k=1) #+ ... + ... + ...


############
## These next 3 variables are the data structures we will use for predictions and evaluation
############
Tdata.train <- as.data.frame(modelData(data.model,
                       data.window=c('1970-01-02','1999-12-31')))
Tdata.eval <- na.omit(as.data.frame(modelData(data.model,
                       data.window=c('2000-01-01','2009-09-15'))))
Tform <- as.formula('T.ind.GSPC ~ .')



###################################################
### The Prediction Models
###################################################

############
## Now we will run 5 different prediction models. After each one, we will retrieve a set of signals 
## that will help us evaluate it's performance. These signals are produced by the sigs.PR() calls,
## and produce a 'precision' and 'recall' value for each of 3 evaluators: 's' (sell), 'b'(buy), and 
## 's+b' (sell + buy). Precision tells us how many times for each buy and sell, the action proved profitable,
## while Recall can be thought of as 'missed opportunities': times we held when we should have acted. 
############
library(nnet)
set.seed(1234)
norm.data <- scale(Tdata.train)

############
## The first model is a neural network. Create a call to nnet with the following components:
##	- Tform as your formula
##	- data consisting of the first 1000 rows of norm.data
##	- A hidden layer size of 10
##	- a decay rate of .01
##	- 1000 maximum iterations
##	- set linout to TRUE, and trace to FALSE
## consult '?nnet' or http://cran.r-project.org/web/packages/nnet/nnet.pdf for reference 
############
nn <- nnet( , , , , , , )
norm.preds <- predict( , ) #Now use that nn to predict the next 1000 values for norm.data


preds <- unscale(norm.preds,norm.data)
sigs.nn <- trading.signals(preds,0.1,-0.1)
true.sigs <- trading.signals(Tdata.train[1001:2000,'T.ind.GSPC'],0.1,-0.1)
sigs1 <- sigs.PR(sigs.nn,true.sigs)
sigs1

############
## Next we run another neural net. Leave all parameters the same as your run above, but this
## time, change the formula to a new one that does the following:
##	- predicts 'signals' as a response, using all other columns of the dataset.
############
library(nnet)
set.seed(1234)
signals <- trading.signals(Tdata.train[,'T.ind.GSPC'],0.1,-0.1)
norm.data <- data.frame(signals=signals,scale(Tdata.train[,-1]))

nn <- nnet( , , , , , , )
preds <- predict(nn,norm.data[1001:2000,],type='class')

sigs2 <- sigs.PR(preds,norm.data[1001:2000,1])
sigs2


############
## Those models proved lackluster in their Precision and Recall values. Let's try a Support Vector Machine.
## Call the svm functions using:
##	- Tform as your formula
##	- data consisting of the first 1000 rows of Tdata.train
##	- A gamma value of .001
##	- A cost of 100
## consult '?svm' or http://cran.r-project.org/web/packages/e1071/vignettes/svmdoc.pdf for reference 
############
library(e1071)
set.seed(1234)
sv <- svm( , , , )
s.preds <- predict( , ) # Once again, predict the next 1000 entries of Tdata.train with your svm

sigs.svm <- trading.signals(s.preds,0.1,-0.1)
true.sigs <- trading.signals(Tdata.train[1001:2000,'T.ind.GSPC'],0.1,-0.1)
sigs3 <- sigs.PR(sigs.svm,true.sigs)
sigs3


############
## That helped! Now let's try another form of svm, the ksvm from the kernlab package
## Call the ksvm functions using:
##	- A function that predicts 'signals' as a response, using all other columns of the dataset.
##	- data consisting of the first 1000 rows of data
##	- A cost of constraints violations value of 10
## consult '?ksvm' or http://cran.r-project.org/web/packages/kernlab/kernlab.pdf for reference 
############
library(kernlab)
set.seed(1234)
data <- cbind(signals=signals,Tdata.train[,-1])
ksv <- ksvm( , , )
ks.preds <- predict( , ) # Predict the next 1000 entries of data with your ksvm

sigs4 <- sigs.PR(ks.preds,data[1001:2000,1])
sigs4


############
## Oops. That made it worse. Let's do one more model: a Multivariate Adaptive Regression Spline (MARS)
## implemented in R through the function earth(). 
## Call earth() using:
##	- Tform as the formula
##	- The first 1000 rows of Tdata.train
############
library(earth)
set.seed(1234)
e <- earth( , )
e.preds <- predict( , ) # One final time, run the prediction on the next 1000 values of Tdata.train

sigs.e <- trading.signals(e.preds,0.1,-0.1)
true.sigs <- trading.signals(Tdata.train[1001:2000,'T.ind.GSPC'],0.1,-0.1)
sigs5 <- sigs.PR(sigs.e,true.sigs)
sigs5

############
## This last section is important so you can persist your information with us.
## Without running the code exactly this way, you will not be able to receive credit for your work.
## Please do not edit this code.
############
results <- data.frame(c(index1,index2,index3))
colnames(results) <- c("output")
write.csv(results, file="results.csv",row.names=FALSE,quote=FALSE)
write(sig1, file="results.csv",append=TRUE)
write(sig2, file="results.csv",append=TRUE)
write(sig3, file="results.csv",append=TRUE)
write(sig4, file="results.csv",append=TRUE)
write(sig5, file="results.csv",append=TRUE)
sink()
