#!/usr/bin/Rscript

#output suppressers
sink("/dev/null")
suppressPackageStartupMessages(library("methods")) 

library(class)
library(ggplot2)

#Retrieve k value and raw csv data from command law
args <- commandArgs(TRUE)
k <- args[1]
trainingData <- args[2]
testData <- args[3]
header <- args[4]

# comma delimited data and no header for each variable
if (header == "header") {
	RawData <- read.table(trainingData,sep = ",",header=TRUE)
	RawTestData <- read.table(testData,sep = ",",header=TRUE)
} else {
	RawData <- read.table(trainingData,sep = ",",header=FALSE)
	RawTestData <- read.table(testData,sep = ",",header=FALSE)
}

if (ncol(RawData) == ncol(RawTestData)){ # Presumes that Testing data contains labels - let's strip those out
	RawTestData[ncol(RawTestData)] = NULL
}

#knn stuff
responseY <- as.matrix(RawData[,ncol(RawData)])
predictorX <- as.matrix(RawData[,1:(ncol(RawData)-1)])

# Finding Principal Components so we can visualize the data
pc.comp <- princomp(scale(predictorX))$scores
pc.comp2 <- princomp(scale(as.matrix(RawTestData[,1:(ncol(RawData)-1)])))$scores


train <- predictorX
test <- RawTestData

#Run the actual knn
knn_results <- knn(train,test,responseY,as.numeric(k))
results <- data.frame(output = knn_results)

#Title of final plot
title <- paste("K-Nearest Neighbors (K=",toString(k),")",sep="")


trainLabels <- RawData[,ncol(RawData)]
testLabels <- as.numeric(as.character(knn_results))
total <- c(trainLabels, testLabels) #combines the labels from the test and training sets for graphing purposes
total <- cut(total, seq(-1,1))

print(trainLabels)

x <- c(pc.comp[,1], pc.comp2[,1])
y <- c(pc.comp[,2], pc.comp2[,2])
pc.comp.v <- princomp(scale(predictorX))$sdev
pc.comp2.v <- princomp(scale(as.matrix(RawTestData[,1:(ncol(RawData)-1)])))$sdev

variance.pc.comp = pc.comp.v^2 / sum(pc.comp.v^2)

features <- c()
for(i in 1:nrow(RawData)){
	if(!(RawData[i,ncol(RawData)] %in% features)){
		features <- c(features, RawData[i, ncol(RawData)])
	}
}


##### CREATE GRAPH ######
xlabel <- paste("Principal Component 1",paste("(",round(variance.pc.comp[1]*100,digits=3),"% Variance)",sep=""),sep=" ")
ylabel <- paste("Principal Component 2",paste("(",round(variance.pc.comp[2]*100,digits=3),"% Variance)",sep=""),sep=" ")

print(length(features))

scatter <- qplot(-1 * x, -1 * y, xlab=xlabel,ylab=ylabel) +
scale_colour_manual('Legend', breaks = total, labels = features, values = rainbow(length(features))) +
geom_point(aes(colour=total)) +
ggtitle(title) +
theme(plot.title = element_text(size = rel(2)))

#write as csv, Galaxy finds it based on the name "results.csv"
write.csv(results, "results.csv", row.names = FALSE, quote=FALSE) #knn contains test labels
png('plot.png') # creates a canvas for plotting
plot(scatter) #draws the graph to "plot.png", which can be found by Galaxy

print(paste(getwd(),"plot.png",sep="/"))

#end suppressers
dev.off()
sink()
